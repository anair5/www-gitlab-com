---
layout: localized-handbook-page-toc
title: "GitLab Subscription Agreement"
description: "Learn more about the GitLab Subscription Agreement"
banner_text: "Cette traduction est fournie à titre informatif uniquement. En cas de divergence entre le texte anglais et cette traduction, la version anglaise prévaudra."
---

## Rubriques
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contrat d'abonnement

Le présent Contrat d'abonnement (le « Contrat ») est conclu entre GitLab Inc., dont les bureaux sont situés au 268 Bush Street, Suite 350, San Francisco, CA 94104, États-Unis [ou toute autre entité désignée comme « GitLab » sur un Bon de commande (cf. ci-dessous), (ci-après « GitLab »)], et la personne physique ou l'entité qui signe ou accepte par voie électronique le présent Contrat, ou tout Bon de commande faisant référence au présent Contrat (ci-après le « Client »). Le présent Contrat est réputé conclu à la première des dates suivantes : (a) lorsque le Client clique sur « Accepter » ou « Oui » sur la page des Conditions du présent Contrat pour obtenir un premier accès au Logiciel ou pour l'utiliser, (b) lorsque GitLab et le Client conviennent d'un Bon de commande faisant référence au présent Contrat, ou (c) lorsque le Logiciel est mis à la disposition du Client (ci-après « Date d'entrée en vigueur »).

- Personne signant au nom de l'entreprise. SI LA PERSONNE QUI CONCLUT LE PRÉSENT CONTRAT AGIT AU NOM D'UNE ENTREPRISE OU D'UNE AUTRE ENTITÉ, ELLE DÉCLARE QU'ELLE DISPOSE DU POUVOIR DE SOUMETTRE CETTE ENTREPRISE ET SES SOCIÉTÉS AFFILIÉES AUX PRÉSENTES CONDITIONS GÉNÉRALES, AUQUEL CAS LE TERME « CLIENT » SE RÉFÈRE À CETTE ENTREPRISE ET À SES SOCIÉTÉS AFFILIÉES.

- Personne non autorisée à signer au nom de l'entreprise. SI LA PERSONNE QUI CONCLUT LE PRÉSENT CONTRAT NE DISPOSE PAS D'UNE TELLE AUTORITÉ OU N'ACCEPTE PAS LES PRÉSENTES CONDITIONS GÉNÉRALES, ELLE NE DOIT PAS ACCEPTER LE PRÉSENT CONTRAT ET NE POURRA UTILISER LES SERVICES OU LE LOGICIEL.

- Personne signant et agissant en son nom propre mais utilisant l'adresse électronique d'une entreprise. SI LA PERSONNE QUI CONCLUT LE PRÉSENT CONTRAT AGIT EN SON NOM PROPRE MAIS UTILISE L'ADRESSE ÉLECTRONIQUE D'UNE ENTREPRISE POUR CE FAIRE, ELLE RECONNAÎT ET ACCEPTE QUE L'UTILISATION DE CETTE ADRESSE ÉLECTRONIQUE PROFESIONNELLE ÉTABLIRA UN COMPTE GITLAB QUI SERA ASSOCIÉ À L'ENTREPRISE CONCERNÉE, ET QUI PEUT ET SERA ENTIÈREMENT TRANSFÉRÉ (CONTRÔLE ET DONNÉES/INFORMATIONS DU COMPTE) À CETTE ENTREPRISE À LA DEMANDE DE CELLE-CI, SANS PRÉAVIS OU RESPONSABILITÉ ENVERS LA PERSONNE. PAR CONSÉQUENTGITLAB RECOMMANDE PAR CONSÉQUENT VIVEMENT D'ÉTABLIR UN COMPTE GITLAB LIÉ À UNE ADRESSE ÉLECTRONIQUE PERSONNELLE AFIN D’ÉVITER TOUTE PERTE DE CONTENU PERSONNEL .


### 1. DÉFINITIONS
L'« Acceptation » d'un Bon de commande intervient à la première des éventualités suivantes : (a) la signature d'un Bon de commande, (b) la référence à un n° de devis de Bon de commande dans un bon de commande ou un document similaire, ou (c) l'utilisation du Logiciel.

Les « Conditions supplémentaires » sont des conditions générales distinctes régissant l'accès et l'utilisation par le Client de certaines (a) fonctionnalités du Logiciel ou (b) Services supplémentaires disponibles à l'achat tels qu’indiqués ici : <https://about.gitlab.com/terms/>.

Le terme « Utilisateur(s) complémentaire(s) » désigne les utilisateurs en plus de ceux prévus dans le cadre d'un Abonnement par le biais d'un Bon de commande signé ou d'un achat sur le portail Web.

Un « Affilié » désigne toute entité contrôlant, contrôlée par, ou sous contrôle commun d'une partie aux présentes, le terme « contrôle » désignant le pouvoir légal de diriger ou de faire diriger la gestion générale de l'entité ou la propriété de plus de 50 % des titres de cette entité avec droit de vote.

Les « Annexes » sont des clauses complétant le présent Contrat précisant les conditions selon lesquelles le Logiciel est proposé au Client. Les Annexes sont considérées comme faisant partie intégrante du Contrat.

Un « Partenaire agréé » est un revendeur ou un distributeur habilité et autorisé à vendre le Logiciel.

Le « Logiciel GitLab Community Edition » désigne le logiciel et les composants open source accessibles au public et développés par la communauté susceptibles d’être fournis avec le Logiciel. Le Logiciel GitLab Community Edition est fourni en tant que Logiciel gratuit (tel que défini auxpar les présentes).

Les « Prestataires » sont définis comme des tiers engagés par le Client pour gérer ou utiliser le Logiciel et uniquement pour son propre compte.

L'« Objet du contrôle » désigne collectivement le Logiciel, tout autre logiciel, tout élément connexe ou tout produit directement lié à ce dernier.

Le « Contenu client » désigne l'ensemble des logiciels, informations, contenus et données fournis par le Client ou en son nom, ou mis à disposition ou autrement distribués par le biais de l'utilisation du Logiciel.

Les « Dossiers clients » désignent collectivement les livres, registres, contrats et comptes relatifs aux paiements dus à GitLab en vertu du présent Contrat.

Les « Services de réussite client » désignent les services de gestion fournis dans le cadre de l'Abonnement, comme indiqué à l'Annexe 1. Les Services de réussite client comprennent la collecte de Données opérationnelles (comme indiqué plus en détail à l'Annexe 1). Les Services de réussite client sont uniquement disponibles aux Clients faisant l’acquisition du Logiciel et ne le sont pas pour les Utilisateurs du Logiciel gratuit.

L'« Assistance client » désigne l'assistance technique relative au Logiciel fourni par GitLab.

Un « Ressortissant désigné » désigne toute personne ou entité figurant sur la liste des ressortissants spécialement désignés dupar le Ministère du Trésor américain (U.S. Department of Treasury) ou figurant dans le tableau des ordonnances de refus du Ministère du Commerce américain (U.S. Department of Commerce).

Le « Prix effectif » désigne le prix effectivement réglé par le Client (Prix catalogue minoré de toute réduction applicable) tel qu'indiqué sur le Bon de commande ou tel qu’indiqué sur le Site Web.

Les « Pays sous embargo » désignent collectivement les pays envers lesquels les États-Unis maintiennent un embargo.

L'« Entreprise » désigne l'organisation, l'entreprise, la société et/ou tout autre type d'entité juridique ou personnes morale faisant l’acquisition du le Logiciel pour l'utiliser en son nom conformément aux Conditions générales du présent Contrat.

Les « Frais » correspondent aux frais indiqués sur le Bon de commande, ou au frais dus immédiatement lors de l'achat via le Site Web.

Le « Logiciel gratuit » désigne une version à fonctionnalités limitées du Logiciel fournie à un Client, un Utilisateur, un utilisateur final, un partenaire ou tout autre tiers, à titre gratuit (ou à un coût considérablement réduit), y compris, mais sans s'y limiter, l'offre au prix le plus bas du Logiciel telle que mise à disposition par GitLab.

Un « Particulier » désigne une personne utilisant le Logiciel pour son propre compte et non pour celui d’une Entreprise. Un Particulier doit être âgé de plus de treize (13) ans.

Le « Prix catalogue » désigne le prix catalogue du Logiciel GitLab, à l'exclusion (le cas échéant) de toute remise indiquée sur un Bon de commande ou sur le Site Web lors de l'achat.

Un « Bon de commande » est un document transactionnel établi entre les parties qui indique le Logiciel et/ou les Services supplémentaires achetés, les conditions d'utilisation, le prix et d'autres informations relatives à la transaction. Pour éviter toute ambiguïté, les parties reconnaissent et acceptent que les conditions générales énoncées au présent Contrat et sur le Bon de commande signé s'appliquent à toutes les questions envisagées par les présentes.

Le « Bon de commande » est un document de traitement d'un Client, ou un document similaire, utilisé par le Client pour démontrer l'approbation interne et/ou la confirmation d'un achat. Toutes les conditions énoncées sur un Bon de commande sont réputées nulles et non avenues, et sont expressément rejetées par les parties.

Le « Logiciel » désigne le logiciel et les autres offres de marque mis à disposition par GitLab ou sa(ses) Société(s) affiliée(s), y compris, mais sans s'y limiter, la Plateforme d'application du cycle de vie DevOps de GitLab ainsi que les Services supplémentaires applicables.

L'« Abonnement » désigne les services, l'assistance et la ou les fonction(s) applicable(s) du Logiciel tel que fourni. Les Abonnements sont proposés avec différentes éditions (niveaux), telles que décrites à l'Annexe 1 et en fonction du nombre d'Utilisateurs.

La « Date de début de l'Abonnement » désigne, sauf accord contraire formulé par écrit, la date de début, (i) indiquée sur un Bon de commande, ou la date à laquelle le Client a accès au Logiciel (la date la plus tardive étant retenue), ou (ii) telle qu'indiquée lors d’une transaction sur le Site Web, que cet achat soit effectué directement auprès de GitLab ou par l'intermédiaire d'un Partenaire agréé.

La « Durée de l'Abonnement » débute à la Date de début de l'Abonnement et se poursuit pendant douze (12) mois, à moins qu'une disposition contraire ne précise cette durée dans un Bon de commande ou un achat sur le Site Web.
Les « Services supplémentaires » désignent des capacités, fonctionnalités, espaces de stockage et/ou d'autres éléments supplémentaires que le Client peut se procurer séparément moyennant des Frais supplémentaires. Ces Services supplémentaires peuvent être achetés au moyen d'un Bon de commande ou sur le Site Web. Les Services supplémentaires achetés sont : 
(i) fournis en tant qu'article distinct sur un Bon de commande ou un achat sur le Site Web, et 
(ii) associés à la Durée de l'Abonnement sous-jacente s'ils ne sont pas achetés à la Date de début de l'Abonnement.

Un « Utilisateur » désigne l'unique Particulier, employé, Prestataire ou tout autre personne ou machine tierce autorisée par le Client (conformément au présent Contrat) nécessitant l'octroi d'un siège au sein de la plateforme d'administration, en mesure d'accéder au Logiciel acheté dans le cadre d'un Abonnement, indépendamment du fait que l'Utilisateur y accède effectivement, ou de la fréquence à laquelle il y accède. Un Utilisateur doit être âgé de plus de treize (13) ans.

Le « Site Web » désigne le site Web de GitLab accessible à l'adresse www.gitlab.com et tous ses sous-domaines, ainsi que l'ensemble du contenu, des services et de la documentation fournis sur ledit Site Web.

### 2. CHAMP D'APPLICATION DU CONTRAT ; CONDITIONS SUPPLÉMENTAIRES
2.1 Le présent Contrat établit un cadre qui permet à GitLab de fournir le Logiciel au Client. Le Logiciel est fourni dans le cadre d'un Abonnement, tel que décrit à l'Annexe 1. Le Logiciel fourni en tant que solution hébergée, ou Logiciel en tant que service (« Logiciel SaaS ») est soumis à l'Annexe 2 ci-jointe intitulée « Offre de Logiciel en tant que service (SaaS) ».
2.2 Des Conditions supplémentaires s'appliquent à certaines fonctionnalités ou à certains Services supplémentaires auxquels le Client accède, qu'il utilise, qu'il active ou qu'il acquière de toute autre manière, notamment la fourniture de fonctionnalités d'intelligence artificielle. Les Conditions supplémentaires prévaudront en cas de conflit entre le présent Contrat et les Conditions supplémentaires en ce qui concerne ces fonctionnalités ou Services supplémentaires.


### 3. PROCESSUS DE COMMANDE
3.1 Le présent Contrat s'applique au Logiciel que le Client acquiert sous licence directement auprès de GitLab, d'une Société affiliée de GitLab ou d'un de ses Partenaires agréés. Pour éviter toute ambiguïté, dans le cas où le Client achète le Logiciel auprès d'un Partenaire agréé, GitLab n'a, sauf accord contraire formulé par écrit entre le Client et GitLab, aucune obligation envers le Client relatives à d'éventuelles conditions générales en dehors du présent Contrat, sauf accord contraire formulé par écrit entre le Client et GitLab.

3.2 Sauf accord contraire formulé par écrit entre le Client et GitLab, les conditions générales du présent Contrat régissent toute utilisation du Logiciel. Les achats du Logiciel peuvent être effectués soit par :
(i) l'achat via le Site Web de GitLab ;
(ii) la signature d'un Bon de commande passé entre GitLab ou une Société affiliée de GitLab ; ou
(iii) l'achat auprès d'un Partenaire agréé.

3.3 GitLab et le Client reconnaissent et acceptent que le Logiciel gratuit puisse être : 
(i) modifié et/ou mis à jour, sans préavis, et 
(ii) limité en termes de possibilités, de fonctionnalités, de maintenance, d'assistance et qu'il puisse contenir d'autres limitations non présentes dans le Logiciel acheté. NONOBSTANT LES CLAUSES « GARANTIE » ET « INDEMNISATION » CI-DESSOUS, LE LOGICIEL GRATUIT ET LE LOGICIEL PROPOSÉ À TITRE D'ESSAI (COMME INDIQUÉ DANS UN BON DE COMMANDE OU LORS D'UN ACHAT SUR LE SITE WEB) SONT FOURNIS « EN L'ÉTAT », SANS AUCUNE GARANTIE, ET GITLAB N'A AUCUNE OBLIGATION D'INDEMNISATION NI RESPONSABILITÉ DE QUELQUE NATURE QUE CE SOIT À L'ÉGARD DE CE LOGICIEL GRATUIT, À MOINS QU'UNE TELLE EXCLUSION DE RESPONSABILITÉ NE SOIT PAS APPLICABLE EN VERTU DE LA LOI EN VIGUEUR, AUQUEL CAS LA RESPONSABILITÉ DE GITLAB À L'ÉGARD DUDIT LOGICIEL GRATUIT NE DÉPASSERA PAS 1 000,00 USD.


### 4. DURÉE ET RÉSILIATION
4.1 Le Contrat prend effet à la Date d'entrée en vigueur et se poursuit jusqu'à ce qu'il soit résilié conformément à la présente Clause 4.

4.2 La Durée de l'Abonnement commence à la Date de début de l'Abonnement et reste en vigueur pendant la durée indiquée sur le Bon de commande (la « Durée initiale ») et se renouvelle automatiquement pour des durées successives de douze (12) mois (chacune étant désignée comme une « Durée de renouvellement ») et pour le même nombre de licences d'Utilisateur achetées au cours de la dernière Durée de l'Abonnement, plus les Utilisateurs supplémentaires activés et/ou utilisés pendant cette Durée de l'Abonnement, à moins que l'une ou l'autre des parties ne notifie son intention de mettre fin au renouvellement dudit Abonnement trente (30) jours avant l'expiration de la Durée de l'Abonnement en cours, ou comme convenu autrement par les parties. Le Client dispose du droit de refuser un tel renouvellement par le biais du Logiciel à compter de la Date de début de l'Abonnement et jusqu'à trente (30) jours avant l'expiration de la Durée de l'Abonnement. Les Abonnements doivent être utilisés pendant la Durée de l'Abonnement et tout Abonnement non utilisé expire.

4.3 L'une ou l'autre des parties peut résilier le présent Contrat et tout Bon de commande signé entre les parties si :

(a) l'autre partie viole matériellement le présent Contrat et ne remédie pas à la violation dans les trente (30) jours suivant la notification écrite ; ou

(b) l'autre partie fait l'objet d'une procédure de faillite ou de toute autre procédure relative à l'insolvabilité, à la mise sous séquestre, à la liquidation ou à une cession au profit de créanciers.

4.4 Si le Client enfreint les dispositions de la Clause 6 (Paiement des Frais), GitLab peut (à sa seule discrétion) 

(a) suspendre la livraison des Abonnements jusqu'à ce que la violation soit corrigée ou 

(b) se prévaloir de tout autre recours en vertu du présent Contrat, y compris la résiliation dudit Contrat en vertu de la Clause 4.3.

4.5 Sauf indication contraire par les présentes, la résiliation du présent Contrat n'affecte pas les Abonnements en cours et le présent Contrat reste pleinement en vigueur jusqu'à l'expiration de la Durée de l'Abonnement alors en vigueur. Dans le cas où le présent Contrat serait résilié par le Client conformément à la Clause 4.3, GitLab remboursera au Client tous les Frais prépayés pour la partie proportionnelle de la Durée de l'Abonnement inutilisée. Si le présent Contrat devait être résilié par GitLab conformément à la présente Clause 4.3, le Client paiera (le cas échéant) tous les Frais impayés couvrant le reste de la Durée de l'Abonnement de tous les Bons de commande, dans la mesure permise par la loi applicable. Pour éviter toute ambiguïté, la résiliation ne libère en aucun cas le Client de son obligation de payer les Frais dus à GitLab pour la période précédant la Date d'entrée en vigueur de la résiliation. Les conditions générales du présent Contrat s'appliqueront à toute Durée de renouvellement, à condition que, sauf indication contraire sur un Bon de commande, un achat sur le Site Web ou tout autre accord écrit entre les parties, le Prix catalogue de GitLab alors en vigueur s'applique à ladite Durée de renouvellement. GitLab se réserve le droit d'augmenter les Frais pour toute Durée de renouvellement en ce qui concerne ses produits et services, y compris le Logiciel et les Services supplémentaires.


### 5. RESTRICTIONS ET RESPONSABILITÉS
5.1 Le Client s'interdit, et ne permettra pas à un tiers (qui n'est pas autrement défini comme un Utilisateur), de procéder aux opérations suivantes :
(i) utiliser le Logiciel à des fins autres que celles spécifiquement autorisées par le présent Contrat ;
(ii) utiliser le Logiciel en violation des Conditions d'utilisation acceptables disponibles sur <https://about.gitlab.com/terms/#current-terms-of-use> ;
(iii) utiliser le Logiciel de façon à permettre à un tiers d'accéder au Logiciel ;
(iv) utiliser le Logiciel à des fins de partage de temps ou de bureau de service (y compris, sans s'y limiter, la sous-licence, la distribution, la vente, la revente de tout Logiciel) ;
(iv) utiliser le Logiciel à des fins autres que son usage interne et celui de ses Sociétés affiliées ;
(vi) utiliser le Logiciel autrement que dans le respect de toutes les lois et réglementations applicables ;
(vii) utiliser le Logiciel de toute manière qui : (a) serait nuisible, frauduleuse, trompeuse, menaçante, abusive, harcelante, délictueuse, diffamatoire, vulgaire, obscène ou calomnieuse (y compris, sans s'y limiter, accéder à un ordinateur, un système informatique, un réseau, un logiciel ou des données sans autorisation, violer la sécurité d'un autre utilisateur ou système, ou tenter de contourner tout processus d'authentification ou de sécurité de l'utilisateur) ; (b) usurperait l'identité de toute personne ou entité, y compris, sans s'y limiter, tout employé ou représentant de GitLab ; (c) comprendrait du contenu illégal en rapport avec l'utilisation du Logiciel SaaS ou (d) introduirait un virus, un cheval de Troie, un ver, une bombe à retardement, un message non sollicité, un message commercial ou un « spam », ou un autre code informatique, fichier ou programme nuisible (y compris, sans s'y limiter, des programmes qui devinent les mots de passe, des décodeurs, des collecteurs de mots de passe, des enregistreurs de frappe, des outils de craquage, des renifleurs de paquets ou des programmes de contournement du chiffrement) ; et
(viii) sauf dans la mesure permise par la loi applicable, désassembler, procéder à une ingénierie inverse ou décompiler le Logiciel ou y accéder pour : (a) créer un produit ou un service concurrentiel, (b) créer un produit ou un service en utilisant des idées, des fonctionnalités, des fonctions ou des graphiques semblables à ceux du Logiciel, (c) copier des idées, des fonctionnalités, des fonctions ou des graphiques du Logiciel, ou (d) déterminer si le Logiciel relève d’un quelconque brevet.

5.2 Rien dans le présent Contrat n'interdit au Client d'utiliser le Logiciel à des fins de test de référence ou d'analyse comparative. Le Client doit se conformer à toutes les lois applicables en matière de confidentialité et de sécurité des données et mettre en place les contrôles technologiques, administratifs et physiques appropriés pour assurer ladite conformité.

5.3 En plus des obligations énoncées à la Clause 5.4, et sous réserve des droits énoncés à la Clause 5.7, le Client doit s'assurer que la collecte des données nécessaires à l'utilisation du Logiciel (les « Données d'Abonnement ») reste inchangée. Un aperçu des données d'Abonnement est disponible à l'adresse <https://metrics.gitlab.com/?q=subscription>.

5.4 Conformément au présent Contrat, GitLab dispose du droit de vérifier par voie électronique (ou par d'autres moyens) et de générer des rapports relatifs à l'installation, à l'accès et à l'utilisation du Logiciel par le Client afin d'assurer le respect des dispositions du présent Contrat. Le Client doit conserver les Dossiers clients pendant toute la durée du présent Contrat et pendant les deux (2) années suivant sa fin. GitLab peut, moyennant un préavis écrit de trente (30) jours adressé au Client pendant les heures normales de bureau du Client et sous réserve des obligations de confidentialité conformes aux normes de l'industrie, engager un auditeur tiers indépendant pour auditer les Dossiers clients uniquement afin de vérifier les sommes redevables en vertu du présent Contrat concernant l'utilisation du Logiciel par le Client. Le Client devra, si un audit laisse apparaître un défaut de paiement, rapidement régler le montant dû à GitLab ainsi que des frais de retard conformément à la Clause 6. GitLab supportera le coût de l'audit à moins que celui-ci ne révèle un défaut de paiement de plus de 5 % pour la période auditée, auquel cas le Client devra payer avec diligence les coûts raisonnables d'audit à GitLab.

5.5 Le Client est tenu responsable des éléments suivants :
(i) le maintien de la sécurité de son compte, de ses mots de passe (y compris, mais sans s'y limiter, les mots de passe administrateur et Utilisateur) et de ses fichiers, ainsi que de toute utilisation du Compte client avec ou sans sa connaissance ou son consentement ; et
(ii) tout acte ou omission effectué par les Prestataires pour le compte du Client. Le Client doit s'assurer que les Prestataires sont soumis à des conditions au moins aussi strictes que celles énoncées par les présentes.

5.6 Gitlab doit, sous réserve du présent Contrat et du Bon de commande applicable, fournir un Support client aux Clients abonnés pendant la Durée de l'Abonnement, sans frais supplémentaires. Les informations concernant le Support client se trouvent à l'Annexe 1, ainsi qu'à l'adresse <https://about.gitlab.com/support>, telles que régulièrement mises à jour.

5.7 Certaines parties du Logiciel sont régies par des licences open source sous-jacentes telles que décrites à l'adresse <https://docs.gitlab.com/ee/development/licensing.html>. Le présent Contrat et les Annexes applicables établissent les droits et obligations associés aux Abonnements et au Logiciel et ne sont pas destinés à limiter les droits du Client vis-à-vis du code logiciel selon les termes d'une licence open source.

5.8 Le Client reconnaît et accepte que :
(i) les noms de compte sont administrés par GitLab selon le principe du « premier arrivé, premier servi » ;
(ii) il est interdit de s'accaparer intentionnellement des noms intentionnellement, d'acheter, de solliciter ou de vendre un nom de compte ; et
(iii) GitLab se réserve, à son entière discrétion, le droit de supprimer, de renommer ou de fermer les comptes inactifs.



### 6. PAIEMENT DES FRAIS
6.1 En ce qui concerne les achats effectués directement auprès de GitLab, tous les Frais d'achat du Site Web sont dus et payables immédiatement.

6.2 En ce qui concerne les achats effectués directement auprès de GitLab, le Bon de commande doit : 
(i) faire référence au présent Contrat ; 
(ii) indiquer la Durée de l'abonnement et les Abonnements en cours d'achat ; et 
(iii) indiquer les Frais applicables pour le ou les Abonnements.

6.3 En ce qui concerne les achats effectués directement auprès de GitLab, le Bon de commande est incorporé au présent Contrat par référence. Par les présentes, les parties acceptent les conditions générales énoncées au présent Contrat et celles figurant sur le Bon de commande, à l'exclusion de toutes autres conditions. Les parties conviennent que toutes les conditions énoncées sur un Bon de commande ou un autre document similaire sont réputées nulles et non avenues et sont expressément rejetées.

6.4 En ce qui concerne les achats effectués directement auprès de GitLab, le Client doit verser à GitLab les Frais applicables, y compris ceux relatifs aux le Services supplémentaires, sans aucun droit de compensation ou de déduction. Tous les paiements seront effectués conformément aux informations de paiement indiquées sur le Bon de commande applicable. Sauf indication contraire : 
(i) GitLab (ou la Société affiliée de GitLab applicable) doit facturer les Frais au Client lors de l'Acceptation du Bon de commande ; et 
(ii) tous les Frais seront dus et payables dans les trente (30) jours suite à la réception de la facture par le Client. Sauf stipulation expresse dans le présent Contrat, aucun des Frais payés ou dus en vertu des présentes (y compris les montants prépayés) n'est remboursable et aucun crédit ne sera dû, y compris, sans s'y limiter, en cas de résiliation du présent Contrat conformément à la Clause 4 des présentes.

6.5 Pendant la Durée de l'Abonnement, le Client peut, sous réserve du présent Contrat, activer et utiliser des Utilisateurs complémentaires. Pour éviter toute ambiguïté, le Client n'a pas le droit de désigner un nombre d'Utilisateurs inférieur au nombre d'Utilisateurs initialement achetés dans le cadre de l'Abonnement, et tous les Utilisateurs complémentaires seront associés à la Durée de l'Abonnement sous-jacente.

6.6 En ce qui concerne les achats effectués directement auprès de GitLab, à la fin de chaque période de trois (3) mois à compter de la Date de début de l'Abonnement (ci-après dénommé « Trimestre » ou « Trimestriel ») et pendant la Durée de l'Abonnement, GitLab doit : (i) générer, conformément à la Clause 5.4, générer un rapport sur le ou les Utilisateurs complémentaires déployés au cours du Trimestre (« Rapport trimestriel d'utilisation »), et (ii)doit facturer le Client au prorata pour la partie restante de la Durée de l'Abonnement, en ce qui concerne le ou les Utilisateurs complémentaires activés ou utilisés au cours du Trimestre, telle qu'enregistrée dans le Rapport trimestriel d'utilisation. Pour éviter toute ambiguïté, le ou les Utilisateurs complémentaires ne seront pas facturés pour le Trimestre au cours duquel ils ont été activés ou utilisés. Un Rapport trimestriel d'utilisation sera généré au cours des trois (3) premiers Trimestres de la Durée de l'Abonnement. À l'expiration de la Durée de l'Abonnement, le renouvellement du Logiciel par le Client doit porter sur le même nombre de licences d'Utilisateur achetées pour la dernière Durée de l'Abonnement, plus les Utilisateurs supplémentaires activés et/ou utilisés pendant ladite Durée de l'Abonnement, sauf accord contraire entre les parties. Tous les montants des Utilisateurs complémentaires identifiés dans le Rapport trimestriel d'utilisation sont considérés comme dus et payables conformément à la présente Clause 6. Le Client devra, au cas où un Rapport trimestriel d'utilisation ne pourrait être généré, signaler et payer ces Utilisateurs excédentaires (tels que définis ci-dessous) conformément à la Clause 6.7. À moins que les parties ne conviennent d'un Prix effectif inférieur au Prix catalogue, tel qu'indiqué sur un Bon de commande ou un achat sur le Site Web, les Utilisateurs complémentaires seront facturés au Prix catalogue dans le cadre du Bon de commande ou lors de l'achat sur le Site Web le plus récent.

6.7 Dans le cas où un Client achèterait le Logiciel auprès d'un Partenaire agréé, ou si GitLab n'était pas en mesure de, (i) vérifier et générer un Rapport trimestriel d'utilisation, ou (ii) collecter les paiements relatifs aux Compléments trimestriels tels que prévus dans le Rapport trimestriel d'utilisation, le Client est tenu de, (a) fournir un rapport au plus tard douze (12) mois après la Date d'entrée en vigueur de l'Abonnement (« Rapport annuel ») de tous les Utilisateurs lors de ladite Durée de l'Abonnement (« Utilisateurs excédentaires »), et (b) payer pour ces Utilisateurs excédentaires, pour les douze (12) mois précédents, au Prix catalogue alors en vigueur pour le Logiciel GitLab. Les Utilisateurs excédentaires soumis au Rapport annuel ne doivent pas inclure de prorata, de compensation ou de déduction pour tenir compte des conditions d'utilisation ou d'autres dispositions. Tous les montants relatifs aux Utilisateurs excédentaires identifiés dans un Rapport annuel sont considérés comme dus et payables conformément à la Clause 6. Le Client sera tenu, au cas où il resterait des Utilisateurs excédentaires à l'expiration d'une Durée de l'Abonnement, de payer pour ces Utilisateurs excédentaires afin de renouveler le Logiciel.

6.8 Tous les Frais impayés sont soumis à des frais s'élevant à un pour cent (1,0 %) par mois, ou le maximum autorisé par la loi, le montant le plus bas étant retenu, plus tous les frais de recouvrement, y compris les honoraires d'avocat raisonnables. Les Frais en vertu du présent Contrat excluent toutes les taxes ou tous les droits, actuellement ou ultérieurement, imposés par une autorité gouvernementale, y compris, mais sans s'y limiter, toute taxe nationale, étatique ou provinciale, taxe de vente, taxe sur la valeur ajoutée, taxe foncière et autres taxes similaires, le cas échéant. Les Frais en vertu du présent Contrat sont payés sans aucune retenue ni déduction. Dans le cas de toute déduction ou obligation de retenue, le Client devra lui-même payer toute retenue requise et ne pas réduire le montant à payer à GitLab à ce titre.


### 7. CONFIDENTIALITÉ
7.1 Chaque partie (la « Partie destinataire ») comprend que l'autre partie (la « Partie divulgatrice ») a divulgué ou peut divulguer des informations relatives à la technologie ou aux activités de la Partie divulgatrice (ci-après dénommées « Informations confidentielles »). Ces Informations confidentielles doivent être : 
(i) identifiées comme telles au moment de leur divulgation ; ou 
(ii) de nature à ce que ces informations ou leur mode de divulgation soient telles qu'une personne raisonnable les comprendrait comme étant confidentielles. Sans limiter la portée de ce qui précède, et sous réserve des licences open source applicables, le Logiciel est considéré comme une Information confidentielle de GitLab.

7.2 La Partie destinataire accepte de : 
(i) ne pas divulguer à un tiers ces Informations confidentielles ; 
(ii) donner accès à ces Informations confidentielles uniquement aux employés en ayant besoin aux fins du présent Contrat ; et 
(iii) prendre les mêmes précautions de sécurité pour se protéger contre la divulgation ou l'utilisation non autorisée de ces Informations confidentielles que la partie applique à ses propres informations confidentielles, mais en aucun cas une partie n'appliquera des précautions moins que raisonnables pour protéger ces Informations confidentielles.

7.3 La Partie divulgatrice accepte que la Clause 7.2 ne s'applique pas à l'égard de toute information pour laquelle la Partie destinataire peut documenter : 
(i) qu'elle est ou devient accessible au public sans aucune action ou participation de la Partie destinataire ; ou 
(ii) qu'elle était en sa possession ou connue d'elle avant sa réception de la Partie divulgatrice ; ou 
(iii) qu'elle lui a été légitimement divulguée, sans restriction, par un tiers, ou 
(iv) qu'elle a été développée indépendamment sans utiliser d'Informations confidentielles de la Partie divulgatrice.

7.4 Les obligations des parties en matière de protection des Informations confidentielles resteront en vigueur pendant une période de trois (3) ans à compter de la réception desdites Informations confidentielles et survivront à toute résiliation ou expiration du présent Contrat.

7.5 Rien dans le présent Contrat n'empêchera la Partie destinataire de divulguer des Informations confidentielles en vertu d'une ordonnance judiciaire ou gouvernementale, à condition que la Partie destinataire donne à la Partie divulgatrice, lorsque cela est légalement possible, un préavis raisonnable avant une telle divulgation pour lui permettre, aux frais de la Partie divulgatrice, une possibilité raisonnable de demander un traitement confidentiel ou une ordonnance de protection s'y rapportant avant que la Partie destinataire ne fasse une telle divulgation.

7.6 Chaque partie reconnaît et accepte que l'autre peut subir un préjudice irréparable en cas de violation des termes de la présente Clause 7 et que cette partie disposera du droit de demander une injonction (sans la nécessité de dépôt) en cas de violation.

7.7 Les deux parties disposent du droit de divulguer des Informations confidentielles dans le cadre : (i) d'une demande d’information d'une autorité gouvernementale (à condition que cette partie déploie des efforts raisonnables pour obtenir un traitement confidentiel ou une ordonnance de protection), ou (ii) de divulgations faites à des investisseurs ou acquéreurs potentiels, à condition que les Informations confidentielles soient à tout moment protégées d'une manière non moins stricte que celle énoncée dans la présente Clause 7.

7.8 GitLab peut collecter des données liées au taux de réponse global et à d'autres mesures globales des performances du Logiciel et de l'utilisation dudit Logiciel par le Client, et établir des rapports à cet égard. Vous trouverez une présentation de ces données à l'adresse https://about.gitlab.com/handbook/legal/privacy/customer-product-usage-information/. Nonobstant ce qui précède, GitLab n'identifiera pas le Client à un tiers comme étant la source de ces données sans le consentement écrit préalable du Client.

### 8. DROITS DE PROPRIÉTÉ INTELLECTUELLE
8.1 Sous réserve des conditions générales du présent Contrat, GitLab accorde par les présentes au Client et à ses Sociétés affiliées une licence limitée, non exclusive, non transférable et n’autorisant pas la sous-licence permettant aux Utilisateurs du Client et de ses Sociétés affiliées d'utiliser, de reproduire, de modifier et d'afficher le code du Logiciel au niveau sélectionné par le Client, ou indiqué dans le Bon de commande, ou encore d'en préparer des œuvres dérivées, uniquement pour : (i) son utilisation interne dans le cadre du développement du logiciel du Client et/ou de ses Sociétés affiliées et (ii) le nombre d'Utilisateurs pour lesquels le Client a payé GitLab. Nonobstant toute stipulation contraire, le Client convient que GitLab et/ou ses concédants de licence (le cas échéant) conservent tous les droits sur tous les Logiciels incorporés dans ces modifications et/ou correctifs, et que tous ces Logiciels ne peuvent être utilisés, copiés, modifiés, affichés, distribués ou autrement exploités qu'en pleine conformité avec le présent Contrat et avec un Abonnement valable pour le nombre correct d'Utilisateurs.

8.2 Sauf stipulation expresse des présentes, GitLab (et ses concédants de licence, le cas échéant) conserveront tous les droits de propriété intellectuelle liés au Logiciel et à toutes les suggestions, idées, demandes d'amélioration, évaluations ou autres recommandations fournies par le Client, ses Sociétés affiliées, ses Utilisateurs ou tout tiers relatives au Logiciel (ci-après dénommés « Matériels d'évaluation »), qui sont par les présentes attribués à GitLab. Pour éviter toute ambiguïté, les Matériels d'évaluation n'incluent pas d'Informations confidentielles du Client ni de propriété intellectuelle de ce dernier. Le présent Contrat ne constitue pas une vente du Logiciel et ne confère au Client aucun droit de propriété sur le Logiciel ou lié à celui-ci, ni tout autre droit de propriété intellectuelle.

8.3 Le Client ne doit pas supprimer, modifier ou masquer les avis de droit d'auteur de GitLab (ou de ses concédants de licence), les mentions d'exclusivité, les attributions de marques de commerce ou de service, les marques de brevet ou d'autres indices indiquant la propriété ou la contribution de GitLab (ou de ses concédants de licence) du Logiciel.

8.4 Sous réserve de la Clause 8.5, le Client déclare qu'il sera responsable de tous les droits sur le Contenu du Client et qu'il les conservera, sous réserve d'une licence limitée accordée à GitLab nécessaire à la fourniture du Logiciel par GitLab ainsi qu'à son développement et à son amélioration.

8.5 Si le Client applique une licence au Contenu du Client accessible au public dans le Logiciel, le Client (i) octroie des licences pour ce Contenu du Client selon les termes de la licence applicable ; et (ii) déclare que le Client dispose de droits suffisants sur ce Contenu du Client pour le faire.

8.6 Le Client accorde à GitLab le droit d'utiliser le nom et le logo de l'entreprise du Client dans les supports de marketing et de promotion, notamment les publications de résultats et les conférences téléphoniques sur les résultats, sous réserve des directives relatives à la marque et aux marques de commerce du Client fournies à GitLab, en tant que de besoin.


### 9. GARANTIE
9.1 Pendant la Durée de l'Abonnement, GitLab déclare et garantit que : (i) sa compétence est suffisante pour conclure le présent Contrat, (ii) le Logiciel sera fourni de manière professionnelle par du personnel qualifié et (iii) elle utilisera les méthodes standard du secteur commercial conçues pour veiller à ce que le Logiciel fourni au Client n'inclue aucun code informatique ou autre technique, dispositif ou instruction informatique, y compris, sans s'y limiter, ceux connus sous le nom de dispositifs de désactivation, de chevaux de Troie ou de bombes à retardement, intentionnellement conçus pour perturber, désactiver, nuire, infecter, frauder, endommager ou autrement entraver de quelque manière que ce soit le fonctionnement d'un réseau, d'un programme informatique ou d'un système informatique ou de tout composant de celui-ci, notamment sa sécurité ou ses données d'Utilisateurs.

9.2 Si GitLab devait ne pas respecter les garanties de la présente Clause 9, le Client pourrait rapidement aviser GitLab par écrit de cette non-conformité. GitLab devra, dans les trente (30) jours suivant la réception de cette notification écrite, corriger la non-conformité ou fournir au Client un plan pour la corriger. Le Client pourra, si la non-conformité n'est pas corrigée ou si un plan raisonnablement acceptable pour la corriger n'est pas établi pendant cette période, résilier le présent Contrat et recevoir un remboursement au prorata de la partie inutilisée de la Durée de l'abonnement en tant que seul et unique recours pour cette non-conformité.

9.3 SAUF STIPULATIONS PRÉCISES DES PRÉSENTES, LE LOGICIEL, LES SERVICES SUPPLÉMENTAIRES, LES INFORMATIONS CONFIDENTIELLES ET TOUT CE QUI EST FOURNI DANS LE CADRE DU PRÉSENT CONTRAT SONT FOURNIS « EN L'ÉTAT », SANS GARANTIE D'AUCUNE SORTE. GITLAB ET SES CONCÉDANTS DE LICENCE DÉCLINENT PAR LES PRÉSENTES TOUTE GARANTIE, EXPRESSE OU IMPLICITE, Y COMPRIS, SANS S'Y LIMITER, TOUTES LES GARANTIES IMPLICITES DE VALEUR MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER, DE TITRE DE PROPRIÉTÉ ET D'ABSENCE DE CONTREFAÇON.


### 10. INDEMNISATION
10.1 GitLab assurera la défense du Client dans le cadre de toute demande, réclamation, poursuite ou procédure intentée contre celui-ci par un tiers alléguant que le Logiciel (à l'exclusion du Logiciel gratuit comme indiqué à la Clause 3.3) fourni par GitLab enfreint ou détourne le brevet ou les droits d'auteur de ceudit tiers (ci-après une « Demande à l'encontre du Client »). GitLab indemnisera et couvrira le Client contre tous les dommages-intérêts, honoraires d'avocat dans une limite raisonnable et coûts définitivement mis à la charge du Client à la suite d'une Demande à l'encontre de ce dernier, ou au titre des sommes versées par celui-ci en vertu d'un règlement approuvé (par écrit) par GitLab, à condition que le Client : (i) avise rapidement GitLab par écrit de la Demande à son encontre ; (ii) apporte à GitLab toute aide raisonnable aux frais de GitLab et (iii) donne à GitLab le contrôle exclusif de la défense et du règlement de ladite Demande, sachant que GitLab ne sera en mesure de régler aucune Demande à l'encontre du Client sans le dégager inconditionnellement le Client de toute responsabilité à cet égard. Les obligations qui précèdent ne s'appliquent pas si : (v) la Demande à l'encontre du Client découle de la modification du Logiciel par ledit Client du Logiciel, en tout ou en partie, ou d'une telle modification effectuée sur les instructions du Client, après sa livraison par GitLab ; (w) la Demande à l'encontre du Client découle de l'utilisation ou de la combinaison du Logiciel, en tout ou partie, avec d'autres produits, processus ou matériaux que GitLab n'a pas fournis lorsque la violation présumée se rapporte à une telle combinaison ; (x) le Client continue l'activité de violation présumée après en avoir été informé ou après avoir été informé des modifications qui auraient permis de l'éviter ; (y) la Demande à l'encontre du Client résulte de logiciels que GitLab n'a pas créés ou (z) la Demande à l'encontre du Client résulte de la violation par le Client du présent Contrat et/ou des Bons de commande applicables. Nonobstant ce qui précède, en cas de Demande à l'encontre du Client, GitLab se réserve le droit, à sa discrétion et à ses frais : (a) de modifier le Logiciel pour le rendre non contrefaisant, à condition qu'il n'y ait pas de perte substantielle de fonctionnalité ; (b) de régler cette demande en donnant au Client le droit de continuer à utiliser le Logiciel ou (c) si, de l'avis raisonnable de GitLab, ni l'option (a), ni l'option (b) ne sont réalisables sur le plan commercial, de résilier la licence du Logiciel et de rembourser la somme versée par le Client pour ledit Logiciel au prorata de la partie inutilisée de la Durée d'abonnement.

10.2 Le Client assurera la défense de GitLab et de ses Sociétés affiliées dans le cadre de toute demande, réclamation, poursuite ou procédure intentée contre GitLab par un tiers alléguant : (i) que tout Contenu du Client ou l'utilisation par le Client du Contenu du Client avec le Logiciel ou tout logiciel (ou combinaison de logiciels) fourni par le Client et utilisé avec le Logiciel, enfreint ou détourne les droits de propriété intellectuelle de ce tiers ou (ii) résultant de l'utilisation par le Client du Logiciel de manière illégale ou en violation du Contrat, de la documentation applicable ou du Bon de commande (chaque cas étant une « Demande à l'encontre de GitLab »). Le Client garantira et couvrira GitLab contre tous les dommages-intérêts, honoraires d'avocat dans une limite raisonnable et frais définitivement mis à la charge de GitLab à la suite d'une Demande à l'encontre de GitLab ou au titre de sommes versées par celle-ci en vertu d'un règlement approuvé (par écrit) par le Client d'une Demande à l'encontre de GitLab, à condition que GitLab : (x) avise rapidement le Client par écrit de la Demande à l'encontre de GitLab, (y) donne au Client le contrôle exclusif de la défense et du règlement de la Demande à l'encontre de GitLab (sachant que le Client ne sera en mesure de régler aucune Demande à l'encontre de GitLab sans dégager inconditionnellement GitLab de toute responsabilité) à cet égard et (z) apporte au Client toute aide raisonnable, aux frais du Client. Les obligations de défense et d'indemnisation qui précèdent ne s'appliquent pas si une Demande à l'encontre de GitLab résulte de la violation par GitLab du présent Contrat et/ou du Bon de commande applicable.

10.3 La présente Clause 10 (Indemnisation) énonce l'unique responsabilité de la partie qui indemnise et le recours exclusif de la partie qui indemnise contre l'autre partie au titre de toute demande de tiers énoncée dans la présente Clause.


### 11. LIMITATION DE RESPONSABILITÉ
11.1 DANS LA MESURE PERMISE PAR LA LOI APPLICABLE, LA RESPONSABILITÉ DES PARTIES OU DE LEURS CONCÉDANTS DE LICENCE NE SERA PAS ENGAGÉE AU TITRE DE DOMMAGES-INTÉRÊTS INDIRECTS, PUNITIFS, ACCESSOIRES, SPÉCIAUX, CONSÉCUTIFS, DE MANQUE À GAGNER, DE BÉNÉFICES ANTICIPÉS, DE PERTE D'AFFAIRES OU DE PERTE DE VENTES, QU'ILS SOIENT FONDÉS SUR LA RESPONSABILITÉ CONTRACTUELLE, DÉLICTUELLE (Y COMPRIS POUR FAUTE), STRICTE OU AUTRE, MÊME SI CETTE PARTIE A ÉTÉ INFORMÉE DE LA POSSIBILITÉ DE DOMMAGES-INTÉRÊTS.

11.2 DANS LA MESURE PERMISE PAR LA LOI APPLICABLE, LA RESPONSABILITÉ TOTALE DE CHAQUE PARTIE, DE SES SOCIÉTÉS AFFILIÉES ET DE SES CONCÉDANTS DE LICENCE RÉSULANT DU PRÉSENT CONTRAT OU S'Y RAPPORTANT, QU'ELLE SOIT FONDÉE SUR LA RESPONSABILITÉ CONTRACTUELLE, DÉLICTUELLE (Y COMPRIS POUR FAUTE), STRICTE OU AUTRE, NE DÉPASSERA PAS, DANS L'ENSEMBLE, LA SOMME TOTALE VERSÉE PAR LE CLIENT OU SES SOCIÉTÉS AFFILIÉES EN VERTU DES PRÉSENTES AU COURS DES DOUZE MOIS PRÉCÉDANT LE PREMIER INCIDENT DONT DÉCOULE LA RESPONSABILITÉ. LES LIMITATIONS QUI PRÉCÈDENT S'APPLIQUENT NONOBSTANT TOUTE DÉFAILLANCE DE L'OBJECTIF ESSENTIEL DE TOUT RECOURS LIMITÉ, MAIS SANS LIMITER LES OBLIGATIONS DE PAIEMENT DU CLIENT OU DE SES SOCIÉTÉS AFFILIÉES EN VERTU DE LA CLAUSE « PAIEMENT DES FRAIS » PRÉCITÉE.


### 12. AFFAIRES CONCERNANT LE GOUVERNEMENT DES ÉTATS-UNIS
12.1 Nonobstant toute autre disposition, le Client reconnaît que l'Objet du contrôle est soumis aux lois et réglementations sur le contrôle du commerce, y compris le Règlement sur l'administration des exportations des États-Unis (« EAR ») et divers programmes de sanctions administrés par l'Office of Foreign Assets Control (« OFAC ») des États-Unis. Le Client ne doit pas exporter, réexporter ou transférer l'Objet du contrôle, excepté dans la mesure autorisée par ces lois et règlements.

12.2 Sans limiter ce qui précède, le Client ne doit pas exporter, réexporter ou transférer l'Objet du contrôle i) vers un pays ou une région sous embargo, y compris Cuba, l'Iran, la Corée du Nord, la Syrie ou les régions de Crimée, de Donetsk et de Louhansk en Ukraine, ii) vers une partie identifiée sur la Liste des ressortissants spécialement désignés de l'OFAC, sur les Listes des entités, des personnes non vérifiées ou des personnes privées du Bureau de l'industrie et de la sécurité, ou iii) pour une utilisation finale ou un utilisateur final interdit par le 15 CFR 744, y compris, sans s'y limiter, les activités de prolifération liées aux armes nucléaires, aux missiles ou aux armes chimiques et biologiques.

12.3 L'utilisation du Logiciel vaut déclaration et garantie que le Client, le personnel du Client ou les Prestataires ne se trouvent pas dans un Pays sous embargo, ni ne sont sous le contrôle d'un ressortissant ou d'un résident d'un Pays sous embargo ou encore d'un Ressortissant désigné.

12.4 Tel que défini à l'article 2.101 du FAR, tout logiciel et toute documentation fournis par GitLab sont des « articles commerciaux » et, conformément à l'article 252.2277014 (a)(1) et (5) du DFAR, sont réputés être des « logiciels informatiques commerciaux » et de la « documentation sur les logiciels informatiques commerciaux ». Conformément à l'article 227.7202 du DFAR et à l'article 12.212 du FAR, toute modification, reproduction, publication, exécution, affichage ou divulgation d'un tel logiciel commercial ou d'une telle documentation logicielle commerciale par le gouvernement des États-Unis sera régie uniquement par les termes du présent Contrat et sera interdite, excepté dans la mesure expressément autorisée par les termes du présent Contrat.

### 13. FORCE MAJEURE
13.1 GitLab et le Client ne seront pas responsables de tout manquement ou retard dans l'exécution de leurs obligations non financières respectives, dans la mesure où ce manquement ou ce retard est causé, directement ou indirectement, par un incendie, une inondation, un tremblement de terre, des explosions, des éléments de la nature, des catastrophes naturelles, des actes ou règlements d'organismes gouvernementaux, une contamination nucléaire, chimique ou biologique, des ordonnances judiciaires découlant de circonstances autres qu'une violation du présent Contrat par la Partie défaillante (telle que définie ci-dessous), des actes de guerre, de terrorisme, des émeutes, des troubles civils, des rébellions ou des révolutions, des grèves, des lock-out ou des conflits du travail, des épidémies ou par tout autre événement indépendant du contrôle de GitLab ou du Client. La partie qui n'est pas en mesure d'exécuter ses obligations est dénommée la « Partie défaillante ». Un tel événement donnant lieu au manquement ou au retard est dénommé par les présentes « Cas de force majeure ».

13.2 La Partie défaillante sera dispensée de toute nouvelle exécution de ses obligations non financières concernées par un tel Cas de force majeure aussi longtemps que ledit Cas de force majeure se poursuivra et que la Partie défaillante continuera d'apporter la diligence et les efforts nécessaires sur le plan commercial pour reprendre l'exécution de ses obligations. La Partie défaillante peut résilier le présent Contrat si le Cas de force majeure se poursuit pendant une période de trente (30) jours.

13.3 Excepté dans les cas expressément excusés à la présente Clause 13, chaque partie continuera d'exécuter ses obligations respectives en vertu du présent Contrat pendant un Cas de force majeure.


### 14. SÉCURITÉ/PROTECTION DES DONNÉES
14.1 Sans limiter les obligations de GitLab énoncées à la Clause 7 (Confidentialité), il incombe à GitLab d'élaborer et de gérer un programme de sécurité de l'information adéquat sur le plan commercial et conçu pour : (i) assurer la sécurité et la confidentialité du Contenu du Client ; (ii) le protéger contre les menaces ou dangers anticipés mettant en péril sa sécurité ou son intégrité ; (iii) le protéger contre les accès ou l'utilisation non autorisés et (iv) veiller à ce que tous les sous-traitants de GitLab, le cas échéant, se conforment à tout ce qui précède. Les garanties du programme de sécurité de l'information de GitLab ne doivent en aucun cas être moins strictes que les garanties de sécurité de l'information auxquelles GitLab a recours pour protéger ses propres données sensibles sur le plan commercial. Le Client doit mettre en œuvre des mesures de sécurité et anti-virus adéquates sur le plan commercial lors de l'accès au Logiciel et son utilisation afin d'empêcher tout accès ou toute utilisation non autorisés de celui-ci ; il doit aviser GitLab rapidement de toute utilisation ou tout accès non autorisé dont il a connaissance.

14.2 À l'égard de la protection des informations, la Déclaration de confidentialité de GitLab, disponible ici <https://about.gitlab.com/privacy/>, s'applique. Si le présent Contrat est conclu pour le compte d'une Entreprise, les modalités de l'avenant relatif au traitement des données disponible à l'adresse <https://about.gitlab.com/handbook/legal/data-processing-agreement/> (« DPA ») sont intégrées par référence aux présentes et s'appliquent dans la mesure où le Contenu du Client comprend des Données à caractère personnel, telles que définies dans le DPA. Dans la mesure où GitLab traite des Données à caractère personnel provenant de l'Espace économique européen (EEE), du Royaume-Uni et de la Suisse, les Clauses contractuelles types s'appliquent, tel qu'indiqué dans le DPA, s'appliquent. Aux fins des Clauses contractuelles types, le Client et ses Sociétés affiliées concernées sont individuellement exportateur de données ; l'acceptation du présent Contrat par le Client et la signature d'un Bon de commande par une Société affiliée concernée valent signature des Clauses contractuelles types.

14.3 Les parties reconnaissent et conviennent que (i) le Logiciel n'est pas conçu aux fins de stocker, traiter, compiler ou transmettre des Données sensibles (telles que définies par les présentes) et (ii) le Client ne doit pas utiliser le Logiciel, ou autrement fournir à GitLab sans consentement écrit préalable, des Données sensibles en vertu du présent Contrat. « Données sensibles » désigne : (a) les catégories particulières de données visées dans le règlement de l'Union européenne 2016/679, article 9, paragraphe 1, ou toute législation ultérieure ; (b) les données sur les patients, les informations médicales ou d'autres renseignements relatifs à la santé protégés régis par la loi des États-Unis sur la portabilité et la responsabilité en matière d'assurance maladie (Health Insurance Portability and Accountability Act) (telle que modifiée et complétée) (« Loi HIPAA ») ; (c) les données de carte de crédit, de débit, d'autre carte de paiement ou d'autres données de compte financier, y compris les numéros de compte bancaire ou d'autres informations financières personnellement identifiables ; (d) les numéros de sécurité sociale, les numéros de permis de conduire ou d'autres numéros d'identification administratifs ; (e) d'autres informations soumises à une réglementation ou à une protection en vertu de lois spécifiques telles que la loi des États-Unis de 1998 sur la protection de la vie privée des enfants en ligne (Children's Online Privacy Protection Act) ou la loi des États-Unis Gramm-Leach-Bliley (Gramm-Leach-Bliley Act) (« Loi GLBA ») (ou des règles ou règlements connexes) ; (f) toute donnée similaire à celles précitées qui sont protégées en vertu de lois étrangères ou nationales. Le Client reconnaît en outre que le Logiciel et les fonctionnalités y étant associées ne sont pas destinés à répondre à des obligations légales pour ces utilisations, y compris les exigences des Lois HIPAA et GLBA, et que GitLab n'est pas un associé commercial au sens de la Loi HIPAA. Par conséquent, nonobstant toute autre stipulation du présent Contrat, GitLab n'assume aucune responsabilité au titre des Données sensibles traitées dans le cadre de l'utilisation du Logiciel par le Client.

14.4 Dans la mesure où le Client a des Utilisateurs du Logiciel SaaS résidant en République populaire de Chine, le Client déclare et garantit qu'il a respecté toutes les exigences d'un « sous-traitant de données à caractère personnel », tel que ce terme est défini dans la Loi de la République populaire de Chine sur la protection des informations personnelles (« Loi PIPL »). Cela inclut l'obligation de donner un préavis adéquat et d'obtenir tous les consentements nécessaires des utilisateurs concernés avant que GitLab puisse transférer et traiter des données à caractère personnel à l'étranger ou avant que les sous-traitants tiers de GitLab puissent effectuer des transferts et le traitement ultérieurs. En outre, le Client garantit, le cas échéant, qu'il ne transférera pas de Données à caractère personnel sans une évaluation de la sécurité effectuée par l'administration du cyberespace de Chine, tel qu'énoncé dans la Loi PIPL. Aucun élément de la présente Clause ne limite les obligations de GitLab ou du Client en vertu du DPA.


### 15. DISPOSITIONS DIVERSES

15.1 Si une stipulation du présent Contrat est jugée inapplicable ou nulle, elle sera limitée ou éliminée dans la mesure minimale nécessaire pour que le présent Contrat demeure de plein effet et exécutoire.

15.2 Le présent Contrat n'est pas cessible, transférable et ne peut faire l'objet d'une sous-licence par l'une des parties sans le consentement écrit préalable de l'autre partie, celui-ci ne devant pas être refusé ou repoussé sans motif raisonnable ; sous réserve que l'une ou l'autre des parties puisse transférer et/ou céder les présentes à un successeur en cas de vente de la totalité ou de la quasi-totalité de son entreprise ou de ses actifs auxquels le présent Contrat se rapporte.

15.3 Sauf disposition contraire dans la présente Clause 15.3, Le présent Contrat constitue la déclaration complète et exclusive de l'accord mutuel des parties ; il remplace et annule tous contrats, communications et autres accords antérieurs, écrits et oraux, relatifs à l'objet des présentes. Toutes les dérogations et modifications doivent être effectuées par écrit et signées ou autrement convenues par chaque partie, sauf stipulation contraire des présentes. Nonobstant toute disposition contraire de ce qui précède, (a) GitLab peut, de temps à autre, proposer certaines fonctionnalités ou produits expérimentaux ou versions bêta dans le Logiciel et (b) le Client reconnaît et accepte que (i) l'accès et l'utilisation des fonctionnalités ou produits expérimentaux ou versions bêta seront régis par le Contrat de test disponible à l'adresse suivante : <https://about.gitlab.com/terms/#current-terms-of-use> et (ii) l'accès ou l'utilisation par le Client de ces fonctionnalités ou produits expérimentaux ou versions bêta constituera l'acceptation par le Client de ce Contrat de test.

15.4 Aucune relation d'agence, société de personnes, coentreprise ou relation d'emploi n'est créée à la suite du présent Contrat ; aucune des parties n'a le pouvoir de lier l'autre de quelque manière que ce soit.

15.5 Dans toute action ou procédure visant à faire valoir des droits en vertu du présent Contrat, la partie gagnante sera en droit de recouvrer les frais juridiques et les honoraires d'avocat.

15.6 Toutes les notifications en vertu du présent Contrat seront effectuées par écrits et seront réputées avoir été dûment données lors de leur réception, si remises en personne ; lorsque leur réception est confirmée par voie électronique, si elles sont transmises par télécopieur ou par courrier électronique et lors de la réception, si elles sont envoyées par courrier certifié ou recommandé (accusé de réception demandé), affranchissement prépayé. Toute notification donnée à GitLab doit également inclure la transmission d'une copie à <legal@gitlab.com>.

15.7 En plus des droits accumulés avant la résiliation, les stipulations des Clauses 3.3 et 5 à 15 resteront en vigueur et de plein effet après la résiliation ou l'expiration du présent Contrat.

15.8 Le présent Contrat sera régi par les lois de l'État de Californie, États-Unis, sans égard à ses dispositions relatives aux conflits de lois. Les tribunaux fédéraux et étatiques siégeant dans le comté de San Francisco, Californie, États-Unis, auront compétence exclusive et seront le ressort habilité à l'égard de tout litige découlant de l'objet du présent Contrat ou s'y rapportant. La Convention des Nations Unies sur les contrats de vente internationale de marchandises est expressément rejetée par les parties à l'égard du présent Contrat et des transactions envisagées par les présentes. GitLab peut fournir des traductions du présent Contrat ou d'autres conditions ou politiques. Les traductions sont fournies à des fins d'information uniquement et en cas d'incohérence ou de conflit entre une traduction et la version anglaise, la version anglaise prévaudra.



## ANNEXE 1 : Abonnements GitLab

Les Frais relatifs aux Abonnements sont fonction du nombre d'Utilisateurs et du niveau d'assistance et/ou de fonctionnalité applicable du Logiciel, tel qu'indiqué dans le tableau ci-dessous. Si le Client ne respecte pas suffisamment les spécifications ou instructions écrites des ingénieurs de service de GitLab, relatives à tout ticket ou toute requête d'assistance (y compris, sans s'y limiter, si le Client n'effectue pas de sauvegardes du Contenu du client ou des versions du Logiciel) (chacune de ces situations dénommée individuellement, un « Ticket d'assistance »), GitLab peut cesser ses obligations d'assistance envers le Client à l'égard de ce Ticket d'assistance après avoir donné à celui-ci un préavis écrit de quinze (15) jours si le Client n'est pas en mesure de remédier à cette non-conformité dans le délai de préavis.


#### ABONNEMENTS ET NIVEAUX D'ASSISTANCE

| Abonnement* | Niveau d'assistance (_Délai de première réponse_) | Détails de l'assistance |
|----------------------------------------|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| Forfiat Gratuit _(anciennement « Core » ou « Free »)_ | Forum de la communauté GitLab |                                                                                                        |
| Starter _(anciennement « Basic » ou « Bronze »)_ | Assistance standard de GitLab | Assistance 24 h/24, 5 j/5 avec délai de réponse le jour ouvré suivant (accord de niveau de service (SLA) 24 h/24) Envoi des tickets à <https://support.gitlab.com> |
| Premium _(anciennement Premium ou Silver)_ | Assistance prioritaire (en fonction de l'incidence de l'assistance**) | Cf. la présentation de l'assistance prioritaire <https://support.gitlab.com> |
| Ultimate _(anciennement Gold ou Ultimate)_ | Assistance prioritaire (en fonction de l'incidence de l'assistance**) | Cf. la présentation de l'assistance prioritaire <https://support.gitlab.com> |
*Remarque : les noms des Abonnements sont sujets à changement, mais l'Abonnement concerné pour cette édition reste le même pendant la Durée de l'Abonnement.


**Les catégories d'incidence de l'assistance sont définies à l'adresse suivante : 

**PRÉSENTATION DE L'ASSISTANCE PRIORITAIRE :** <https://about.gitlab.com/support/#priority-support>

**SERVICES DE RÉUSSITE CLIENT**
Les Services de réussite client comprennent une assistance supplémentaire à l'égard de l'utilisation du Logiciel GitLab par le Client. Les Services de réussite client sont fournis gratuitement. Vous pouvez consulter une présentation de ces Services ici : <https://about.gitlab.com/services/customer-success-services/>. Le Client reconnaît et accepte que des données et informations supplémentaires (« Données opérationnelles ») seront collectées aux fins de lui donner accès aux Services de réussite client. Une présentation des Données opérationnelles est disponible ici :<https://gitlab-org.gitlab.io/growth/product-intelligence/metric-dictionary/?data_category=operational>.



## ANNEXE 2 : Le Logiciel en tant qu'offre de service (SaaS)
À l'égard de l'achat et/ou de l'utilisation du Logiciel SaaS par le Client, les conditions supplémentaires suivantes s'appliquent.

#### ACCESSIBILITÉ
GitLab mesurera l'accessibilité du Logiciel SaaS et en rendra compte à l'aide d'outils d'instrumentation et d'observation spécialement conçus pour procurer une mesure représentative de l'accessibilité du service. Vous pourrez consulter le statut récent, les références à la définition des mesures de disponibilité et les rapports historiques sont disponibles sur le site de l'état du système GitLab, situé à l'adresse <https://about.gitlab.com/handbook/engineering/monitoring/#gitlabcom-service-level-availability>, ou accessibles par un lien depuis ce site.

#### RÉSILIENCE
GitLab concevra et mettra en œuvre une infrastructure cloud sous-jacente dotée d'une résilience adéquate sur le plan commercial pour tous les services de données, de calcul et de réseau. Au minimum, GitLab mettra en œuvre le niveau documenté le plus élevé de l'« Architecture de référence GitLab », tel que précisé à l'adresse <https://docs.gitlab.com>.

#### SAUVEGARDES
GitLab mettra en œuvre un système adéquat sur le plan commercial de technologie et de processus de sauvegarde des données pour garantir que les sources de données primaires restent récupérables en cas de défaillances du système.

#### SURVEILLANCE ET INTERVENTION EN CAS D'INCIDENTS
GitLab utilisera un système d'outils d'instrumentation et d'observation pour veiller à la détection et à l'annonce des comportements du système susceptibles de limiter l'utilisation du Logiciel SaaS. GitLab utilisera également des pratiques du secteur raisonnables pour mettre à disposition du personnel d'ingénierie approprié aux fins d'interventions en cas d'incident.

#### MISES À JOUR ET MISES À NIVEAU
GitLab mettra à jour le logiciel SaaS à mesure que des mises à jour seront accessibles et lorsqu'il sera raisonnablement possible de les mettre en œuvre. Les mises à jour de calendrier et de processus seront effectuées à la discrétion de GitLab.

#### MAINTENANCE PLANIFIÉE DU SYSTÈME
GitLab effectuera occasionnellement une maintenance planifiée du système qui impliquera une limitation de l'utilisation d'une partie ou de la totalité des composants du Logiciel SaaS, ou une réduction considérable de ses composants et fonctions pendant la période de maintenance planifiée du système. GitLab donnera un préavis de dix (10) jours ouvrés pour toutes les activités de maintenance planifiée du système. GitLab adoptera une approche proactive pour minimiser le besoin de cette maintenance et limitera la maintenance planifiée du système à moins de quatre (4) heures par mois civil. Nonobstant ce qui précède, en cas de situation d'urgence ou de ticket urgent susceptible de nuire aux clients de GitLab, GitLab est en droit d'effectuer une maintenance non planifiée pour remédier à cette situation. Pour éviter toute ambiguïté, cette maintenance non planifiée doit : (i) être limitée aux problèmes susceptibles de nuire aux clients ; et (ii) être effectuée de manière à perturber le moins possible les clients.

#### SUSPENSION DU SERVICE
GitLab se réserve le droit de suspendre le service du Logiciel SaaS si : (i) le Client ne respecte pas le Contrat et la présente Annexe, (ii) le Client dépasse les limites d'application fixées ou (iii) des demandes ou des utilisations considérées comme malveillantes sont déterminées comme provenant de comptes, de membres du personnel ou de systèmes du Client.






